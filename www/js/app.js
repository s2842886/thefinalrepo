// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var martialManager = angular.module('martialManager', ['ionic','firebase']);

martialManager.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});


martialManager.config(function($stateProvider,$urlRouterProvider) {
  
  $stateProvider.state('menu', {
      cache: false,
      url: "/menu",
      abstract: true,
      templateUrl: "menu.html",
      controller: 'AccountCtrl'
  })
  $stateProvider.state('menu.account', {
      cache: false,
      url: "/account",
      views: {
          'menuContent': {
          templateUrl: "account.html",
          controller: 'AccountCtrl'
        }
      }
  })

  
  $stateProvider.state("menu.home", {
      cache: false,
      url: "/home",
      views: {
          'menuContent': {
            templateUrl: "home.html",
            controller: 'AccountCtrl'
          }
      }    
  })



  $stateProvider.state("menu.studList", {
    url: "/studList",
    views: {
      'menuContent': {
        templateUrl: "studList.html",
        controller: 'AccountCtrl'
      }
    }
  })

  $stateProvider.state("menu.userDetails", {
    url: "/userDetails",
    views: {
      'menuContent': {
        templateUrl: "userDetails.html",
        controller: 'userDetailController'
      }
    }
  })

  $stateProvider.state("menu.singleStudent", {
    cache: false,
    url: "/:id",
    views: {
      'menuContent': {
        templateUrl: "singleStud.html",
        controller: "studentController"
      }
    }
  })
  $stateProvider.state("menu.add", {
    cache: false,
    url: "/add",
    views: {
      'menuContent': {
        templateUrl: "add.html",
        controller: "addStudentController"
      }
    }
  })
  $stateProvider.state("menu.del", {
    cache: false,
    url: "/del",
    views: {
      'menuContent': {
        templateUrl: "delStud.html",
        controller: "deleteController"
      }
    }
  })
  $stateProvider.state("menu.edit", {
    cache: false,
    url: "/edit",
    views: {
      'menuContent': {
        templateUrl: "edit.html",
        controller: "editController"
      }
    }
  })
  $stateProvider.state("menu.one", {
    cache: false,
    url: "/edit/:id",
    views: {
      'menuContent': {
    templateUrl: "editOne.html",
    controller: "studentEditController"
      }
    }
  });
  
  $stateProvider.state("login", {
    cache: false,
    url: "/login",
    templateUrl: "login.html",
    controller: 'AccountCtrl'
  });

 
  
  $urlRouterProvider.otherwise("/login");
  
});