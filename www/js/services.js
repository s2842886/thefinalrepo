martialManager.factory('authService', function($firebase) {
    var ref = new Firebase("https://brilliant-fire-2080.firebaseio.com/");
    return {
        ref: function () {
          return ref;
        }
    }
});

martialManager.factory('studentService',function($firebaseArray) {
    var fb = new Firebase("https://brilliant-fire-2080.firebaseio.com/");
    var studs = $firebaseArray(fb);
    var studentService = {
        all: studs,
        get: function(studId) {
            return studs.$getRecord(studId);
        }        
    };
    return studentService;
});

martialManager.factory('userService', function($firebaseArray) {
    var details = new Firebase("https://brilliant-fire-2080.firebaseio.com/userDetails");
    var users = $firebaseArray(details);
    var userService = {
        all: users,
        get: function(userId) {
            return users.$getRecord(userId);
        }
    };
    return userService;
});




         
            
          
          
          